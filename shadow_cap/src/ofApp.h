#pragma once

#include "ofMain.h"

#include "ofxGui.h"
#include "ofxKinectV2.h"
#include "ofxOpenCv.h"
#include "ofxSyphon.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
    void draw_kinect_buffer(ofEventArgs & args);
    
    void define_color();
    void snap_contours();
    void draw_contours();
    void draw_shadows();
    void snap_shadows();
    void render_cali_img();
    void change_mode();
    
    ofxKinectV2 kinect;
    
    bool debug;
    bool cur_con, pre_con;
    bool cur_bg, pre_bg;
    
    ofxCvGrayscaleImage depth;
    ofxCvGrayscaleImage thres;
    ofxCvGrayscaleImage cali;
    ofPixels cali_pix;
    vector<ofxCvGrayscaleImage> shadows;
    ofxCvBlob blob;
    ofxCvContourFinder contour_finder;
    vector<ofPolyline> plines;
    
    ofxPanel gui;
    ofParameterGroup cv_grp;
    ofParameter<int> threshold;
    ofParameter<int> blob_num;
    ofParameter<int> min_blob_size;
    ofParameter<int> max_blob_size;
    
    ofParameterGroup k_grp;
    ofParameter<bool> enable_cali;
    ofParameter<bool> update_cali;
    ofParameter<int> top_col;
    
    ofParameterGroup d_grp;
    ofParameter<bool> vFlip;
    ofParameter<bool> reduce;
    ofParameter<int> line_w;
    ofParameter<bool> black_bg;
    ofParameter<bool> contour;
    ofParameter<int> fade;
    ofParameter<bool> auto_clear;
    ofParameter<bool> auto_snap;
    
    ofxSyphonServer server;
    ofxSyphonClient client;
    ofFbo fbo;
    
    bool start;
    int mode;
    vector<string> modes;
};
