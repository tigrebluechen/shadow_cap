#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    ofSetVerticalSync(false);
    ofBackground(0);
    kinect.open();
    
    depth.allocate(512, 424);
    thres.allocate(512, 424);
    
    cali_pix.allocate(512, 424, 1);
    
    gui.setup();
    cv_grp.setName("openCV");
    cv_grp.add(threshold.set("threshold", 0, 0, 255));
    cv_grp.add(blob_num.set("blob num", 2, 1, 10));
    cv_grp.add(min_blob_size.set("min blob size", 100, 0, 2000));
    cv_grp.add(max_blob_size.set("max blob size", 2000, 1000, 512*424));
    gui.add(cv_grp);
    
    k_grp.setName("kinect");
    k_grp.add(kinect.minDistance);
    k_grp.add(kinect.maxDistance);
    k_grp.add(enable_cali.set("enable cali", true));
    k_grp.add(update_cali.set("update cali", true));
    k_grp.add(top_col.set("top color", 100, 0, 255));
    gui.add(k_grp);
    
    d_grp.setName("display mode");
    d_grp.add(vFlip.set("vFlip", true));
    d_grp.add(reduce.set("reduce", false));
    d_grp.add(line_w.set("stroke", 1, 1, 5));
    d_grp.add(black_bg.set("black_bg (W)", false));
    d_grp.add(contour.set("contour", false));
    d_grp.add(fade.set("fade", 5, 1, 10));
    d_grp.add(auto_clear.set("auto clear (Q)", true));
    d_grp.add(auto_snap.set("auto snap", false));
    gui.add(d_grp);
    
    gui.loadFromFile("settings.xml");
    
    server.setName("oF Output");
    client.setup();
    fbo.allocate(ofGetWidth(),ofGetHeight());
    
    debug = true;
    cur_con = contour;
    cur_bg = black_bg;
    
    start = false;
    mode = 0;
    
    modes.push_back("pure shadow");
    modes.push_back("white shadow");
    modes.push_back("white contour");
    modes.push_back("black contour");
    modes.push_back("snapshot");
    modes.push_back("black screen");
}

//--------------------------------------------------------------
void ofApp::update(){
    kinect.update();
    ofSetWindowTitle(modes[mode]);
    
    pre_con = cur_con;
    cur_con = contour;
    
    pre_bg = cur_bg;
    cur_bg = black_bg;
    
    if(pre_con == false && cur_con == true){
        plines.clear();
    }
    
    if(cur_bg != pre_bg){
        for(int i = 0; i < shadows.size(); i++){
            shadows[i].invert();
        }
    }
    
    if(update_cali)
        render_cali_img();
    
    depth.setFromPixels(kinect.getDepthPixels());
    thres = depth;
    if(enable_cali)
        thres += cali;
    thres.threshold(threshold);
}

void ofApp::define_color(){
    //bg
    if(black_bg)
        ofBackground(0);
    else
        ofBackground(255);
    
    //blend mode
    if(black_bg){
        ofEnableBlendMode(OF_BLENDMODE_ADD);
    } else {
        thres.invert();
        ofEnableBlendMode(OF_BLENDMODE_MULTIPLY);
    }
    
    //color
    if(contour){ // contour
        if(black_bg)
            ofSetColor(255,40);
        else
            ofSetColor(0, 40);
    } else { // shadow
        if(auto_snap){
            if(black_bg)
                ofSetColor(255,40);
            else
                ofSetColor(255,140);
        } else {
            ofSetColor(255);
            auto_clear = false;
        }
    }
}

//--------------------------------------------------------------
void ofApp::draw(){
    fbo.begin();
    ofClear(0);
    define_color();
    
    ofSetLineWidth(line_w);
    
    if(contour){
        draw_contours();
    } else {
        draw_shadows();
    }
    fbo.end();
    if(vFlip)
        fbo.draw(0, 0, ofGetWidth(), ofGetHeight());
    else
    fbo.draw(ofGetWidth(),0, -ofGetWidth(), ofGetHeight());
    
    server.publishScreen();
    
    if(debug)
        gui.draw();
    if(contour == true && !start){
        ofDrawBitmapString("press s to start", ofGetWidth()/2, ofGetHeight()/2);
    } else if(mode == 5){
        ofDrawBitmapString("PARTICLE!!!!!!!", ofGetWidth()/2, ofGetHeight()/2);
    }
}

void ofApp::snap_contours(){
    int nblobs = contour_finder.nBlobs;
    if(plines.size() > 3 && auto_clear){
        plines.erase(plines.begin());
    }
    if(ofGetFrameNum()%fade == 0)  {
        contour_finder.findContours(thres, min_blob_size, max_blob_size, blob_num, true);
        for(int i = 0; i < contour_finder.nBlobs; i++){
            ofPolyline pl;
            for(int j = 0; j < contour_finder.blobs[i].nPts; j++){
                pl.addVertex(contour_finder.blobs[i].pts[j]*2);
            }
            plines.push_back(pl);
        }
    }
}

void ofApp::draw_contours(){
    if(auto_snap)
        snap_contours();
    for(int i = 0; i < plines.size(); i++){
        plines[i].draw();
    }
    if(!start){
        ofDisableBlendMode();
        ofSetColor(0);
        ofDrawRectangle(0,0,ofGetWidth(),ofGetHeight());
    }
}

void ofApp::snap_shadows(){
    if(shadows.size() > 5 && auto_clear){
        shadows.erase(shadows.begin());
    }
    shadows.push_back(thres);
}

void ofApp::draw_shadows(){
    if(auto_snap && ofGetFrameNum()%fade == 0)
        snap_shadows();
    for(int i = 0; i < shadows.size(); i++){
        if(reduce)
            shadows[i].draw(ofGetWidth()*3/8.0, ofGetHeight()*3/4.0,
                            ofGetWidth()/4.0, ofGetHeight()/4.0);
        else
            shadows[i].draw(0,0, ofGetWidth(), ofGetHeight());
    }
    if(!start){
        ofDisableBlendMode();
        ofSetColor(0);
        ofDrawRectangle(0,0,ofGetWidth(),ofGetHeight());
    }
}

void ofApp::draw_kinect_buffer(ofEventArgs & args){
    ofDisableBlendMode();
    depth.draw(0, 0, 256, 212);
    thres.draw(0, 212, 256,212);
    contour_finder.draw(0,212,246,212);
    cali.draw(0, 424, 256, 212);
}

void ofApp::render_cali_img(){
    for(int i = 0; i < 512*424; i++){
        int y = i / 512;
        float gray_val;
        gray_val = ofMap(y, 0, 424, top_col, 0, true);
        ofColor c(gray_val);
        cali_pix.setColor(i, c);
    }
    cali.setFromPixels(cali_pix);
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    switch(key){
        case ' ':
            if(!auto_snap){
                if(contour)
                    snap_contours();
                else
                    snap_shadows();
            }
            break;
        case 'd':
            debug = !debug;
            break;
        case 'c':
            shadows.clear();
            plines.clear();
        case 's':
            start = true;
            break;
        /*
        case '0':
            black_bg = false;
            contour = false;
            auto_clear = true;
            fade = 1;
            vFlip = true;
            break;
        case '1':
            black_bg = true;
            contour = false;
            fade = 1;
            vFlip = false;
            break;
        case '2':
            start = false;
            black_bg = true;
            contour = true;
            auto_clear = false;
            fade = 3;
            break;
        case '3':
            black_bg = false;
            contour = true;
            break;
        case '4':
            auto_snap = false;
            contour = false;
            shadows.clear();
            plines.clear();
            break;
        case '5':
            black_bg = true;
            shadows.clear();
            break;
         */
        
        case OF_KEY_RIGHT:
            if(mode < 5)
                mode++;
            change_mode();
            break;
        case OF_KEY_LEFT:
            if(mode > 0)
                mode--;
            change_mode();
            break;
        case 'q':
            auto_clear = !auto_clear;
            break;
        case 'w':
            black_bg = !black_bg;
        default:
            break;
    }
}

void ofApp::change_mode(){
    switch(mode){
        case 0:
            black_bg = false;
            contour = false;
            auto_clear = true;
            fade = 1;
            vFlip = true;
            break;
        case 1:
            black_bg = true;
            contour = false;
            fade = 1;
            vFlip = false;
            break;
        case 2:
            start = false;
            black_bg = true;
            contour = true;
            auto_clear = false;
            fade = 3;
            break;
        case 3:
            black_bg = false;
            contour = true;
            break;
        case 4:
            auto_snap = false;
            contour = false;
            shadows.clear();
            plines.clear();
            break;
        case 5:
            black_bg = true;
            shadows.clear();
            break;
            break;
            
    }
    
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
    
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
