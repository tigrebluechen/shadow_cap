#include "ofMain.h"
#include "ofApp.h"

//========================================================================
int main( ){
    ofGLFWWindowSettings settings;
    //settings.resizable = false;
    settings.width = 1350;
    settings.height = 900;
    //settings.setGLVersion(4, 0);
    settings.setPosition(ofVec2f(256,0));
    settings.windowMode = OF_WINDOW;
    shared_ptr<ofAppBaseWindow> mainWindow = ofCreateWindow(settings);
    shared_ptr<ofApp> mainApp(new ofApp);
    
    settings.width = 256;
    settings.height = 636;
    settings.setPosition(ofVec2f(0,0));
    settings.resizable = false;
    // uncomment next line to share main's OpenGL resources with gui
    settings.shareContextWith = mainWindow;
    shared_ptr<ofAppBaseWindow> kinectWindow = ofCreateWindow(settings);
    kinectWindow->setVerticalSync(false);
    
    //mainApp->setupGui();
    ofAddListener(kinectWindow->events().draw,mainApp.get(),&ofApp::draw_kinect_buffer);
    
    ofRunApp(mainWindow, mainApp);
    ofRunMainLoop();
}
