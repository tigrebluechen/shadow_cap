#version 400 core
layout(location = 0) out vec4 fragColor;
in vec2 coord;
uniform sampler2DRect tex;


void main(){
  vec4 color = texture(tex,coord);
    if(color.r > 0.8){
        discard;
    }
}
